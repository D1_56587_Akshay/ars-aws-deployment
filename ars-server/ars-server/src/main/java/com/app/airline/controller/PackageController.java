package com.app.airline.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.airline.dtos.Response;
import com.app.airline.pojos.Packages;
import com.app.airline.services.PackageServiceImpl;

@RestController
@CrossOrigin("*")

public class PackageController {
	
	@Autowired 
	private PackageServiceImpl packageService;
	
	@GetMapping("/getAllPackages")
	public ResponseEntity<?> getAllPackages(){
		List<Packages> list=packageService.getAllPackages();
		System.out.println(list);
		if(list!=null)
			return Response.success(list);
		else
			return Response.error("No Packages Found");
	}
	// Get Packages by ID
	@GetMapping("/{id}")
	public ResponseEntity<?> FindPackagesById(@PathVariable("id") int id) {
		try {
			Packages result = packageService.findPackageById(id);
			if (result == null)
				return Response.error("Packages by this id does not exists");
			return Response.success(result);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

//// Find all Packages
//	@GetMapping("/")
//	public ResponseEntity<?> Findall() {
//		try {
//			List<Package> result = packageService.findAllPackages();
//			if (result == null)
//				return Response.error("No Packages exists");
//			return Response.success(result);
//		} catch (Exception e) {
//			return Response.error(e.getMessage());
//		}
//	}

// Add Packages
	@PostMapping("/add")
	public ResponseEntity<?> addPackages(@Valid @RequestBody Packages Package) {
		try {
			Packages p = packageService.addPackage(Package);
			if (p == null)
				return Response.error("Failed to add Packages");
			return Response.success(p);
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Update Packages
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePackages(@PathVariable("id") int id, @Valid @RequestBody Packages Packages) {
		try {
			Packages p = packageService.findPackageById(id);
			if (p == null)
				return Response.error("Package by this id does not exists");
			else {
				p.setSeatType(Packages.getSeatType());
				p.setFood(Packages.getFood());
				p.setBeverages(Packages.getBeverages());
				p.setPackageFare(Packages.getPackageFare());
				p.setBaggage(Packages.getBaggage());
				Packages p1 = packageService.updatePackage(p);
				return Response.success(p1);
			}
		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}

// Delete Packages by Id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePackages(@PathVariable("id") int id) {
		try {
			Packages result = packageService.findPackageById(id);
			if (result == null)
				return Response.error("Package by this id does not exists");
			else {
				int x = packageService.deletePackageById(id);
				return Response.success(x + " Package deleted");
			}

		} catch (Exception e) {
			return Response.error(e.getMessage());
		}
	}
}
