
package com.app.airline.services;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.airline.dao.UserDao;
import com.app.airline.dtos.Credentials;
import com.app.airline.pojos.User;

@Transactional
@Service
public class UserServiceImpl {
	@Autowired
	private UserDao userDao;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	

//For Sign In
	public User findUserByEmailAndPassword(Credentials cred) {
		User dbuser = userDao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		if (dbuser != null && passwordEncoder.matches(rawPassword, dbuser.getPassword()))
			return dbuser;
		return null;
	}

//For Sign Up
	public int saveUser(User newuser) {
		String rawPassword = newuser.getPassword();
		String encodedPassword = passwordEncoder.encode(rawPassword);
		newuser.setPassword(encodedPassword);
		return userDao.addUser(newuser.getFirstName(), newuser.getLastName(), newuser.getEmail(),
				newuser.getPassword());
	}

	public User findUserById(int userId) {
		return userDao.findById(userId);
	}

	public List<User> findAllUsers() {
		
		return userDao.findAll();
	}

	public User findUserByEmail(String email) {
		return userDao.findByEmail(email);
	}

	public int addUser(User user) {
		return userDao.addUser(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword());
	}

	public User updateUser(int id, User user) {
		User usr = this.findUserById(id);
		if (usr != null) {
			usr.setFirstName(user.getFirstName());
			usr.setLastName(user.getLastName());
			return userDao.save(usr);
		} else
			return null;
	}

	public int deleteUserById(int id) {
		
		if (userDao.existsById(id)) {
			userDao.deleteById(id);
			return 1;
		} else
			return 0;
	}
	
	public User findUser(String firstName, String lastName, String email) {
		return userDao.findByFirstNameAndLastNameAndEmail(firstName, lastName, email);
	}
	public User saveOrUpdate(User user) {
		User user2=userDao.findByFirstNameAndLastNameAndEmail(user.getFirstName(), user.getLastName(), user.getEmail());
		
		if(user2==null) {
			return userDao.save(user);
		}else {
			user2.setPassword(user.getPassword());
			
			return userDao.save(user2);
		}
	}
	
	// Change password
		public Object changePassword(int id, String currentPass, String newPass) {
			
			User user = this.findUserById(id);
			System.out.println(user);
			if (user != null) {
				if(passwordEncoder.matches(currentPass, user.getPassword()))
				{
				String rawpassword = newPass;
				String encodedpass = passwordEncoder.encode(rawpassword);
				user.setPassword(encodedpass);
				return userDao.save(user);
			} else
				return "Current password is wrong";
		}
			else
				return null;
		}
		
		// Change Forgotten password
		public Object changeForgotPassword(User currentUser, String newPass) {
			String rawpassword = newPass;
			String encodedpass = passwordEncoder.encode(rawpassword);
			currentUser.setPassword(encodedpass);
			return userDao.save(currentUser);
		}
}
