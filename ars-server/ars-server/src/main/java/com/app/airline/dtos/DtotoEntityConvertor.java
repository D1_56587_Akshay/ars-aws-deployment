package com.app.airline.dtos;

import org.springframework.stereotype.Component;

import com.app.airline.pojos.Flight;
import com.app.airline.pojos.Schedule;

@Component
public class DtotoEntityConvertor {
	
	public Flight toFlightEntity(FlightDto fDto) {
		Flight entityFlight=new Flight();
		entityFlight.setFlightId(fDto.getFlightId());
		//entityFlight.setFlightId(fDto.getFlightId());
		entityFlight.setAirlineName(fDto.getAirlineName());
		entityFlight.setDepartureAirportName(fDto.getDepartureAirportName());
		entityFlight.setArrivalAirportName(fDto.getArrivalAirportName());
		entityFlight.setSourceCity(fDto.getSourceCity());
		entityFlight.setDestinationCity(fDto.getDestinationCity());
		entityFlight.setBusinessClassFare(fDto.getBusinessClassFare());
		entityFlight.setEconomyClassFare(fDto.getEconomyClassFare());
		entityFlight.setBusinessClassCapacity(fDto.getBusinessClassCapacity());
		entityFlight.setEconomyClassCapacity(fDto.getEconomyClassCapacity());
		entityFlight.setFlightTotalCapacity(fDto.getFlightTotalCapacity());
		return entityFlight;
	}
	
	public Schedule toScheduleEntity(FlightDto flightDto) {
		Schedule entity=new Schedule();
//		Flight flight= new Flight();
//		flight.setId(0);
		entity.setDepartureDate(flightDto.getDepartureDate());
		entity.setDepartureTime(flightDto.getDepartureTime());
		entity.setArrivalDate(flightDto.getArrivalDate());
		entity.setArrivalTime(flightDto.getArrivalTime());
		entity.setFlightStatus(flightDto.getFlightStatus());
		return entity;
	}

}
