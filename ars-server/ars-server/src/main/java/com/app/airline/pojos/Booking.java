package com.app.airline.pojos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "booking")
public class Booking {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "booking_id")
	private int bookingId;

	@Column(name = "booking_date")
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date bookingDate;

	@Column(name = "passenger_count")
	private int passengerCount;
	
	@Column(name = "fare_per_passenger")
	private double farePerPassenger;

	@Column(name = "seat_no")
	private int seatNo;

	@Column(name = "total_fare")
	private double totalFare;

	@Column(name = "ticket_status")
	private String ticketStatus;

	@ManyToOne
	@JoinColumn(name = "passenger_id")
	private Passenger passenger;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "schedule_id")
	private Schedule schedule;
	
	@ManyToOne
	@JoinColumn(name = "offer_id")
	private Offers offer;
	
	@ManyToOne
	@JoinColumn(name = "package_id")
	private Packages packages;
	
	@ManyToOne
	@JoinColumn(name = "flight_id")
	private Flight flight;

	public Booking() {
		super();
	}

	public Booking(int bookingId, Date bookingDate, int passengerCount, double farePerPassenger, int seatNo,
			double totalFare, String ticketStatus) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.passengerCount = passengerCount;
		this.farePerPassenger = farePerPassenger;
		this.seatNo = seatNo;
		this.totalFare = totalFare;
		this.ticketStatus = ticketStatus;
	}

	public Booking(Date bookingDate, int passengerCount, double farePerPassenger, int seatNo, double totalFare,
			String ticketStatus) {
		super();
		this.bookingDate = bookingDate;
		this.passengerCount = passengerCount;
		this.farePerPassenger = farePerPassenger;
		this.seatNo = seatNo;
		this.totalFare = totalFare;
		this.ticketStatus = ticketStatus;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

//	public int getPassengerCount() {
//		return passengerCount;
//	}

	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}

//	public double getFarePerPassenger() {
//		return farePerPassenger;
//	}

	public void setFarePerPassenger(double farePerPassenger) {
		this.farePerPassenger = farePerPassenger;
	}

	public int getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(int seatNo) {
		this.seatNo = seatNo;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

//	public Offers getOffer() {
//		return offer;
//	}

	public void setOffer(Offers offer) {
		this.offer = offer;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

//	public Packages getPackages() {
//		return packages;
//	}

	public void setPackages(Packages packagee) {
		this.packages = packagee;
	}

	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", bookingDate=" + bookingDate + ", passengerCount=" + passengerCount
				+ ", farePerPassenger=" + farePerPassenger + ", seatNo=" + seatNo + ", totalFare=" + totalFare
				+ ", ticketStatus=" + ticketStatus + "]";
	}
	
	
	
	
}
