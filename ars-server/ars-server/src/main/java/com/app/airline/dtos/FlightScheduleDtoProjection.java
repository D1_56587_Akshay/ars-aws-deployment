package com.app.airline.dtos;


import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class FlightScheduleDtoProjection {

	private int flightId;
	private String airlineName;
	private int scheduleId;
	@Temporal(TemporalType.TIME)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="HH:mm:ss",timezone = "Asia/Kolkata")
	private Date departureTime;
	@Temporal(TemporalType.TIME)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="HH:mm:ss",timezone = "Asia/Kolkata")
	private Date arrivalTime;
	private Date departureDate;
	private Date arrivalDate;
	private String sourceCity;
	private String destinationCity;
	private String duration;
	private double businessClassFare;
	private double economyClassFare;
	private String flightStatus;
	public FlightScheduleDtoProjection() {
		super();
	}
	public FlightScheduleDtoProjection(int flightId, String airlineName, int scheduleId, Date departureTime,
			Date arrivalTime, Date departureDate, Date arrivalDate, String sourceCity, String destinationCity,
			String duration, double businessClassFare, double economyClassFare, String flightStatus) {
		super();
		this.flightId = flightId;
		this.airlineName = airlineName;
		this.scheduleId = scheduleId;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.sourceCity = sourceCity;
		this.destinationCity = destinationCity;
		this.duration = duration;
		this.businessClassFare = businessClassFare;
		this.economyClassFare = economyClassFare;
		this.flightStatus = flightStatus;
	}
	public int getFlightId() {
		return flightId;
	}
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public int getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getSourceCity() {
		return sourceCity;
	}
	public void setSourceCity(String sourceCity) {
		this.sourceCity = sourceCity;
	}
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public double getBusinessClassFare() {
		return businessClassFare;
	}
	public void setBusinessClassFare(double businessClassFare) {
		this.businessClassFare = businessClassFare;
	}
	public double getEconomyClassFare() {
		return economyClassFare;
	}
	public void setEconomyClassFare(double economyClassFare) {
		this.economyClassFare = economyClassFare;
	}
	public String getFlightStatus() {
		return flightStatus;
	}
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}
	@Override
	public String toString() {
		return "FlightScheduleDtoProjection [flightId=" + flightId + ", airlineName=" + airlineName + ", scheduleId="
				+ scheduleId + ", departureTime=" + departureTime + ", arrivalTime=" + arrivalTime + ", departureDate="
				+ departureDate + ", arrivalDate=" + arrivalDate + ", sourceCity=" + sourceCity + ", destinationCity="
				+ destinationCity + ", duration=" + duration + ", businessClassFare=" + businessClassFare
				+ ", economyClassFare=" + economyClassFare + ", flightStatus=" + flightStatus + "]";
	}
	
	
	
	}

