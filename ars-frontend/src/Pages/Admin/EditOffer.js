import { useEffect, useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import HeaderSelector from "../../Components/HeaderSelector"
import Footer from "../../Components/Footer"
import Content4 from "../../Components/Content4"


const EditOffer = () => {
    const [ offers, setOffers] = useState([])
    const navigate = useNavigate()
    
    useEffect(ViewOffers,[])

function ViewOffers(){

    const url=`${URL}/offers/`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success' && result['data'] !=0){
            setOffers(result['data'])
                toast.info('Checkout Special offers')       
        }else{
            toast.warning('No offer available now')
            navigate('/loginhome') 
        }

    })
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <HeaderSelector/>
            <div>
          
            <h2><marquee>We have some exciting offers for you..........!!!</marquee></h2>
            <button className="btn btn-warning btn-lg float-end" onClick={()=>{navigate('/addoffer')}}>Click here to add new offer</button>
            <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr>
                        <th>Offer ID</th>
                        <th>PromoCode</th>
                        <th>Discount</th>
                        <th>Min.Transaction amt.</th>
                        <th>Valid on</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        offers.map( (o) => (
                            <tr key={o.id}>
                               <td>{o.id}</td> 
                               <td>{o.promoCode}</td>
                                <td>{o.discount}</td>
                                <td>{o.minTxnAmount}</td>
                                <td>{o.validOn}</td>
                                <td>
                                    <button className="btn btn-danger"
                                   onClick={()=>{navigate('/deleteoffer',{state:{offerid :o.id}})}} >Delete Offer</button>
                                </td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
            </div>
            <Content4/>
            <Footer/>
        </div>
    )
}

export default EditOffer