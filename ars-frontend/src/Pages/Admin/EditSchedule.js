import { useLocation, useNavigate } from "react-router";
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import HeaderSelector from "../../Components/HeaderSelector";
import Footer from "../../Components/Footer";
import Content4 from "../../Components/Content4";

const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const EditSchedule = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  const { flightschedule } = state
  const [flightStatus, setFlightStatus] = useState('')
  console.log(flightschedule.scheduleId)

  const EditFlightSchedule = () => {
  if (flightStatus.length == 0) {
      toast.warning('Please select flight status')
    } else {
      console.log(flightStatus)
      const body = {
        flightStatus,
      }

      // url to call the api
      const url = `${URL}/schedules/${flightschedule.scheduleId}`

      // http method: post
      // body: contains the data to be sent to the API
      axios.put(url, body).then((response) => {
        // get the data from the response
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          toast.success('Successfully changed flight status')
          // navigate to the signin page
          navigate('/editflight')
        } else {
          toast.error(result['error'])
        }
      })

    }

  }

  return (
    <div style={{ backgroundColor: '#E5E4E2', height: '100%' }}>
      <HeaderSelector />
      <div className="col mt-4 d-flex justify-content-center" style={{color:"#5C0632"}}><h1>Change Flight status</h1></div>
     <hr/>
      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
          <div className="container p-3">
            <div style={styles.div}>
              <table>
                <tbody>
                  <tr style={{ color: "white" }}>
                    <td style={styles.td1}>
                      <label htmlFor="flightid">Selected Schedule ID </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="departdate">Departure date </label>
                    </td>
                  </tr>
                  <tr>
                    <td style={styles.td}>
                    <input value={flightschedule.id} 
                      type="number"
                      readOnly
                      className="form-control"
                    />
                    </td>
                    <td style={styles.td}>
                      <input value={flightschedule.departure_date}
                      id="departdate"
                      readOnly
                        type="date"
                        className="form-control"
                      />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="deptime">Departure time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="arrivaldate">Arrival date</label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input value={flightschedule.departure_time}
                        type="text"
                        id="deptime"
                        readOnly
                        className="form-control"
                      />
                    </td>
                    <td style={styles.td}>
                      <input value={flightschedule.arrival_date}
                        type="date"
                        readOnly
                        id="arrivaldate"
                        className="form-control"
                      />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="arrivaltime">Arrival time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="fstatus">Flight status </label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input value={flightschedule.arrival_time}
                        type="text"
                        id="arrivaltime"
                        readOnly
                        className="form-control"
                      />
                    </td>
                    <td style={styles.td}>
                    <select onChange={(e) => {
                        setFlightStatus(e.target.value)
                      }}
                    >
                        <option>Scheduled</option>
                        <option>Departed</option>
                        <option>Arrived</option>
                        <option>Cancelled</option>
                    </select>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="row">

              <div className="col mt-4 d-flex justify-content-end">
                <button className="btn btn-primary custom-button px-5 justify-content-end"
                  style={{ backgroundColor: '#5C0632' }}
                  onClick={EditFlightSchedule}
                >
                  Change status
                </button>
              </div>
            </div>
          </div>
        </div>
        <Content4/>
        <Footer/>
      </div>
      );
};

      export default EditSchedule