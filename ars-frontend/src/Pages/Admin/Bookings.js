import { useEffect, useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import Content4 from "../../Components/Content4"
import Footer from "../../Components/Footer"
import HeaderSelector from "../../Components/HeaderSelector"

const Bookings = () => {
    const [ bookings, setBookings] = useState([])
    const navigate = useNavigate()
    
    useEffect(ViewBookings,[])

function ViewBookings(){

    const url=`${URL}/user/booking/all`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success' && result['data'] !=0){
            setBookings(result['data'])
                toast.info('Showing available bookings')       
        }else{
            toast.warning('No bookings available now')
            navigate('/adminhome') 
        }

    })
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <HeaderSelector/>
            <div>
          <br/>           
            <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Booking Date</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Flight ID</th>
                        <th>Source city</th>
                        <th>Destination city</th>
                        <th>Departure airport name</th>
                        <th>Arrival airport name</th>
                        <th>Departure Date </th>
                        <th>Departure Time </th>
                        <th>Arrival Date </th>
                        <th>Arrival Time </th>
                        <th>Seat No</th>
                        <th>Total fare</th>
                        <th>Booking Status</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        bookings.map( (b) => (
                            <tr key={b.booking_id}>
                                <td>{b.booking_id}</td> 
                                <td>{b.booking_date }</td> 
                                <td>{b.first_name }</td> 
                                <td>{b.last_name }</td>
                                <td>{b.flight_id  }</td>
                                <td>{b.source_city }</td>
                                <td>{b.destination_city }</td>
                                <td>{b.departure_airport_name }</td>
                                <td>{b.arrival_airport_name  }</td>
                                <td>{b.departure_date }</td>
                                <td>{b.departure_time }</td>
                                <td>{b.arrival_date }</td>
                                <td>{b.arrival_time}</td>
                                <td>{b.seat_no }</td>
                               <td> Rs. {b.total_fare}</td>
                                <td>{b.ticket_status}</td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
            </div>
            <Content4/>
            <Footer/>
        </div>
    )
}

export default Bookings