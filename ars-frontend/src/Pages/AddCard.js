import axios from "axios"
import { useState } from "react"
import { useNavigate } from "react-router"
import { toast } from "react-toastify"
import Footer from "../Components/Footer"
import HeaderSelector from "../Components/HeaderSelector"
import { URL } from "../config"


const AddCard=()=>{
    const navigate=useNavigate()
    const [ nameOnCard,setNameOnCard ] = useState('') 
    const [ cardNo, setCardNo ] = useState(0)
    const [ validityDate, setValidityDate ] = useState('')
    const [ cvv, setCvv ] = useState(0)
    const [ userId, setUserId ] = useState(sessionStorage.getItem('userId'))
    const totalFare=sessionStorage.getItem('totalFare')
    const addCardDetails = () => {
        if(nameOnCard.length == 0){
        toast.warning('Please enter Name On Card')
        }else if (cardNo.length != 12){
            toast.warning('Please enter 12 digit card number')
        }else if (validityDate.length == 0){
            toast.warning('Please enter validity date')
        }else if (cvv.length != 3){
            toast.warning('Please enter 3 digit cvv')
        }else {
            const body = {
                nameOnCard,
                cardNo,
                validityDate,
                cvv,
                userId,    
            }

            const url = `${URL}/paymentcards/add`

            axios.post(url, body).then((response) => {
                const result = response.data
                console.log(result)
                if (result['status'] == 'success'){
                    toast.success('Added your card')

                    navigate('/user/payment', {state:{totalFare:totalFare}})
                }else {
                    toast.error(result['error'])
                }
            })
        }

    }
    return(
        <div>
            <HeaderSelector/>
            <div class="container-xl mt-5 mb-5 d-flex justify-content-center ">
                <h2>Add Payment Card</h2>
            </div>
            <div className="row">
                <div className="col"></div>
            <div className="col">
                    
                        <div className="form">
                            <div className="mb-3">
                             <label className="label-control">
                                 Name on card
                             </label>   
                             <input 
                             onChange={ (e) => {
                                 setNameOnCard(e.target.value)
                             }}
                             type="text"
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <label className="label-control">
                             Card number
                             </label>
                             <input
                             onChange={ (e) => {
                                 setCardNo(e.target.value)
                             }}
                             maxlength="12"
                             type="number"
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             Valid till
                             </label>
                             <input
                             onChange={ (e) => {
                                 setValidityDate(e.target.value)
                             }}
                             type="date"
                             className="form-control"
                             ></input>
                            </div>

                            <div className="mb-3">
                             <label className="label-control">
                             CVV
                             </label>
                             <input
                             onChange={ (e) => {
                                 setCvv(e.target.value)
                             }}
                             type="number"
                             className="form-control"
                             ></input>
                            </div>
                            
                            <div className="mb-3">
                             <button onClick={addCardDetails} className="btn btn-primary">Add card</button>   
                            </div>
                        </div>
                    </div>
                    <div className="col"></div>
                    </div>
                    <Footer/>
        </div>
    )

}

export default AddCard